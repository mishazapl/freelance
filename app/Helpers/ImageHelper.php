<?php
/**
 * Created by PhpStorm.
 * User: mihail
 * Date: 1/3/20
 * Time: 5:56 PM
 */

namespace App\Helpers;


use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Storage;

class ImageHelper
{

    /**
     * @param string $key
     * @param string $path
     * @param null|string $deletePath
     * @return null|string
     */
    public static function save(string $key, string $path, string $deletePath = null): ?string
    {
        if (!request()->filled($key)) {
            return null;
        }

        if (!empty($deletePath)) {
            Storage::delete($deletePath);
        }

        return Storage::put($path, request()->file($key));
    }
}