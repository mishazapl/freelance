<?php

namespace App\Models\Profile;

use App\User;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Database\Eloquent\Relations\HasOne;

/**
 * Class Profile
 * @package App\Models\Profile
 * @method Builder|Profile byUser(int $userId) static
 */
class Profile extends Model
{
    protected $fillable = [
        'user_id',
        'full_name',
        'phone',
        'image',
        'rating',
        'wallet',
    ];

    /**
     * @return HasMany
     */
    public function ratingLatest(): HasMany
    {
        return $this->hasMany(HistoryRating::class, 'profile_id', 'id')
            ->orderBy('created_at', 'desc')->take(10);
    }

    /**
     * @return HasMany
     */
    public function walletLatest(): HasMany
    {
        return $this->hasMany(HistoryWallet::class, 'profile_id', 'id')
            ->orderBy('created_at', 'desc')->take(10);
    }

    /**
     * @return HasOne
     */
    public function user(): HasOne
    {
        return $this->hasOne(User::class, 'id', 'user_id');
    }

    /**
     * @param Builder $builder
     * @param int $userId
     * @return Builder
     */
    public function scopeByUser(Builder $builder, int $userId): Builder
    {
        return $builder->where('user_id', $userId);
    }
}
