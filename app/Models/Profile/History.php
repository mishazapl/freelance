<?php
/**
 * Created by PhpStorm.
 * User: mihail
 * Date: 1/3/20
 * Time: 4:39 PM
 */

namespace App\Models\Profile;


use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasOne;

/**
 * Class History
 * @package App\Models\Profile
 * @method Builder|History historyForPeriod() static
 */
abstract class History extends Model
{
    protected $fillable = [
        'profile_id',
        'score'
    ];

    /**
     * @param Builder $builder
     * @param string $start
     * @param string $end
     * @return Builder
     */
    public function scopeHistoryForPeriod(Builder $builder, string $start, string $end): Builder
    {
        return $builder->whereDate('created_at', '>=', $start)
            ->orWhereDate('created_at', '<=', $end);
    }

    /**
     * @return HasOne
     */
    public function profile(): HasOne
    {
        $this->hasOne(Profile::class, 'id', 'profile_id');
    }
}