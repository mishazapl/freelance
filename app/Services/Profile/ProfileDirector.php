<?php
/**
 * Created by PhpStorm.
 * User: mihail
 * Date: 1/3/20
 * Time: 5:13 PM
 */

namespace App\Services\Profile;


use App\User;
use Illuminate\Support\Facades\DB;

class ProfileDirector
{

    public function buildEmptyProfileForNewUser(array $data): User
    {
        DB::beginTransaction();

        try {

            $builder = new EmptyProfile();
            $user = $builder->createUser($data);
            $profileId = $builder->createProfile($user->id)->id;
            $builder->createHistoryWallet($profileId);
            $builder->createHistoryRating($profileId);

        } catch (\Exception $exception) {
            DB::rollBack();
            throw $exception;
        }

        DB::commit();

        return $user;
    }
}