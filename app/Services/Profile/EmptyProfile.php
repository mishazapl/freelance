<?php
/**
 * Created by PhpStorm.
 * User: mihail
 * Date: 1/3/20
 * Time: 4:59 PM
 */

namespace App\Services\Profile;


use App\Models\Profile\HistoryRating;
use App\Models\Profile\HistoryWallet;
use App\Models\Profile\Profile;
use App\User;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Str;

class EmptyProfile implements ProfileBuilder
{
    /**
     * @param array $data
     * @return User
     */
    public function createUser(array $data): User
    {
        return User::create([
            'name' => $data['name'],
            'email' => $data['email'],
            'password' => Hash::make($data['password']),
            'api_token' => Str::random(80),
        ]);
    }

    /**
     * @param int $userId
     * @return Profile
     */
    public function createProfile(int $userId): Profile
    {
        return Profile::create(['user_id' => $userId]);
    }

    /**
     * @param int $profileId
     */
    public function createHistoryWallet(int $profileId): void
    {
        HistoryWallet::create(['profile_id' => $profileId]);
    }

    /**
     * @param int $profileId
     */
    public function createHistoryRating(int $profileId): void
    {
        HistoryRating::create(['profile_id' => $profileId]);
    }

}