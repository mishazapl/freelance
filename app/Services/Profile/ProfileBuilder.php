<?php
/**
 * Created by PhpStorm.
 * User: mihail
 * Date: 1/3/20
 * Time: 5:05 PM
 */

namespace App\Services\Profile;


use App\Models\Profile\Profile;

interface ProfileBuilder
{
    /**
     * @param int $userId
     * @return Profile
     */
    public function createProfile(int $userId): Profile;

    /**
     * @param int $profileId
     */
    public function createHistoryWallet(int $profileId): void;

    /**
     * @param int $profileId
     */
    public function createHistoryRating(int $profileId): void;
}