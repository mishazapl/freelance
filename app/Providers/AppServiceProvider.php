<?php

namespace App\Providers;

use App\Models\Profile\Profile;
use App\User;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        $this->deleteUser();
    }

    public function deleteUser()
    {
        Profile::deleting(function ($profile) {

            if (!empty($profile->image)) {

                if (Storage::exists($profile->image)) {
                    Storage::delete($profile->image);
                }
            }

        });

        Profile::deleted(function ($profile) {

            User::find($profile->user_id)->delete();

        });
    }
}
