<?php

namespace App\Http\Controllers;

use App\Helpers\ImageHelper;
use Illuminate\Http\Request;

class ProfileController extends Controller
{
    /**
     * @return \Illuminate\Http\JsonResponse
     */
    public function show()
    {
        return response()->json(auth()->user()->profile->load('ratingLatest', 'walletLatest'));
    }

    /**
     * @return \Illuminate\Http\JsonResponse
     */
    public function edit()
    {
        return response()->json(auth()->user()->profile);
    }


    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function update(Request $request)
    {
        $profile = auth()->user()->profile;
        $data = $request->all();
        $data['image'] = ImageHelper::save('image', '/profile/' . $profile->id, $profile->image);
        return response()->json(['result' => $profile->update($data)]);
    }


    /**
     * @return \Illuminate\Http\JsonResponse
     */
    public function destroy()
    {
        $profile = auth()->user()->profile;
        return response()->json(['result' => $profile->delete()]);
    }
}
